provider "aws" {
  region = var.region

  default_tags {
    tags = {
    owner = "maayns"
    expiration_date = "30-02-23"
    bootcamp = "int"
    }
  }
}

provider "helm" {
  kubernetes {
    host                   = module.eks.cluster_endpoint
    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
    exec {
      api_version = "client.authentication.k8s.io/v1beta1"
      args        = ["eks", "get-token", "--cluster-name", module.eks.cluster_name]
      command     = "aws"
    }
  }
}

terraform {
  required_providers {
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.14.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.6.0"
    }
  }
  required_version = "~> 1.0"
}

terraform {
  backend "s3" {
    bucket = "maayans-bucket"
    key    = "terraform.tfstate"
    region = "eu-west-1"
  }
}