data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.19.0"

  name = "${var.my_name}-vpc"
  cidr = var.cidr_block

  azs                 = data.aws_availability_zones.available.names
  #azs                 = ["${var.region}a", "${var.region}b", "${var.region}c"]
  private_subnets     = var.private_subnet_cidr_blocks
  #private_subnet_tags = var.private_subnet_tags
  public_subnets      = var.public_subnet_cidr_blocks
  #public_subnet_tags  = var.public_subnet_tags
  enable_nat_gateway  = true
  single_nat_gateway = true
  one_nat_gateway_per_az = false

  tags = {
    Terraform   = "true"
    Environment = var.tag_environment
  }
}